CC=gcc
CFLAGS=-fopenmp -O3
DEPS =
OBJ = main.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

ln2: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)

clean:
	rm *.o ln2
